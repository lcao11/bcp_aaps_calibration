#!/usr/bin/env python
# coding: utf-8
from __future__ import absolute_import, division, print_function

import dolfin as dl
import sys, os
sys.path.append( os.environ.get('HIPPYLIB_PATH'))
import hippylib as hl
import numpy as np
import matplotlib.pyplot as plt
import math
from scipy.stats import norm

sys.path.insert(0, '../')
from aaps_inference import *
sys.path.insert(0, '../../ok_newton')
from OKNewton import *
sys.path.insert(0, '../../')
from dram import *

STATE = 0
PARAMETER = 1

import os
import argparse

def prior_m(param, data):
    c_1, c_2, sigma_n = param[3], param[4], param[-1]
    return -(-np.mean(data) - 2 * abs(c_1) * c_2 + c_2 + 0.5) / c_1, sigma_n / (400 * c_1)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='AAPS Inference')
    parser.add_argument('--data',
                        default="./data/img_data.npy",
                        type=str,
                        help="The path to data file")

    parser.add_argument('--parameters',
                        default="./data/true_param.npy",
                        type=str,
                        help="The path to true param file")

    parser.add_argument('--particles',
                        default=1,
                        type=int,
                        help="The number of particles for PMMH")

    parser.add_argument('--samples',
                        default=2000,
                        type=int,
                        help="Number of samples for each processer")
                        
    parser.add_argument('--proc_id',
                        default=0,
                        type=int,
                        help="The processor ID")
                        
    parser.add_argument('--out_path',
                        default='./',
                        type=str,
                        help="Relative path to store output files")
    
    parser.add_argument('--out_freq',
                        default=1,
                        type=int,
                        help="The frequency of saving results to .npy files")

    args = parser.parse_args()

    data_file = args.data
    param_file = args.parameters
    image_data = np.load(data_file)
    true_param = np.load(param_file)
    proc_id = args.proc_id
    n_samples = args.samples
    n_particles = args.particles
    out_freq = args.out_freq
    out_path = args.out_path

    settings = {
        "domain_length": 48,
        "n_cells": 150,
        "param_dim": 3,
        "scale": 0.5,
        "correlation_length": 48/150*0.5,
        "n_proc": n_particles,
    }

    m_mean, m_std = prior_m(true_param, image_data)

    domain_length = settings["domain_length"]
    nx = settings["n_cells"]
    mesh = dl.RectangleMesh(dl.Point(0, 0), dl.Point(domain_length, domain_length), nx, nx)

    # Create linear Lagrange finite element
    P1 = dl.FiniteElement("Lagrange", mesh.ufl_cell(), 1)

    # Create the mixed space for the state space
    Vh_STATE = dl.FunctionSpace(mesh, P1 * P1)

    # Create "the real number space" for the model parameters
    param_dim = 3
    Vh_PARAMETER = dl.VectorFunctionSpace(mesh, "R", degree=0, dim=param_dim)

    Vh = [Vh_STATE, Vh_PARAMETER]

    # Create the initial random state object
    scale = settings["scale"]
    correlation_length = settings["correlation_length"]
    state_init = GRF_initial_guess(Vh[STATE], correlation_length, scale)

    # Create the forward problem object
    pde = ok_min_prob(Vh, state_init)

    pde.ok_min_solver.parameters["print_level"] = -1
    x = [pde.generate_state(), pde.generate_parameter()]

    prior = MixedPrior(Vh[PARAMETER])
    index_uniform = [0, 1]
    upper_uniform = np.array([1.0, 1.0])
    lower_uniform = np.array([0.1, 0.1])
    index_gaussian = [2]
    upper_gaussian = np.array([0.0])
    lower_gaussian = np.array([-1./math.sqrt(3)])
    prior.set_uniform_prior(index_uniform, upper_uniform, lower_bound=lower_uniform)
    prior.set_gaussian_prior(index_gaussian, np.array([[m_std]])**2, np.array([m_mean]),\
                             upper_bound = upper_gaussian, lower_bound = lower_gaussian)

    noise, m_true = dl.Vector(), dl.Vector()
    prior.init_vector(noise, "noise")
    prior.init_vector(m_true, 1)

    misfit = aaps_likelihood(Vh[STATE], 100, ratio=1.1, n_h=50, r_max=50)
    sub_image = image_data[image_data.shape[0]//2-50:image_data.shape[0]//2+50,\
              image_data.shape[1]//2-50:image_data.shape[1]//2+50]
    misfit.set_observation_data(sub_image)
    misfit.set_observation_parameters(true_param[3], true_param[5]*4, true_param[6])

    model = hl.Model(pde, prior, misfit)
    forward_mp = ok_forward_parallel(settings)

    proposal_std = np.zeros(3)
    proposal_std[0:2] = [0.05, 0.08]
    proposal_std[2] = m_std
    kernel = PMMHKernel(model, forward_mp, np.diag(proposal_std**2))
    kernel.parameters["non-adaptive_samples"] = 20
    kernel.parameters["maximum_proposals"] = 3
    kernel.parameters["number_of_particles"] = n_particles

    chain = DRAM_MCMC(kernel)
    chain.parameters["number_of_samples"] = n_samples
    chain.parameters["print_progress"] = n_samples//out_freq
    chain.parameters["print_level"] = -1
    chain.parameters["number_of_particles"] = 2

    tracer = Tracer(Vh, chain.parameters["number_of_samples"], chain.parameters["print_progress"], out_path)

    noise, m0 = dl.Vector(), dl.Vector()
    prior.init_vector(noise, 1)
    prior.init_vector(m0, 1)
    # Burn initial guess sample
    for i in range(proc_id):
        hl.parRandom.normal(1, noise)
        chain.consume_random()
    hl.parRandom.normal(1, noise)
    prior.sample(noise, m0)

    print(m0.get_local())

    # Start simulating a MCMC chain!
    n_accept, total_samples, total_fwd_solves = chain.run(m0, tracer)
    tracer.save()
    np.save(out_path + "chain_data.npy", np.array([n_accept, total_samples, total_fwd_solves]))
    


