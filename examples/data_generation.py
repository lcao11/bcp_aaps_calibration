# Author: Lianghao Cao
# Date: 02/20/2022
from __future__ import absolute_import, division, print_function

import dolfin as dl
import hippylib as hl
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.insert(0, '../')
from aaps_inference import *
sys.path.insert(0, '../../ok_newton')
from OKNewton import *

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

STATE = 0
PARAMETER = 1

if __name__ == "__main__":

    out_folder = "./cover_plot/" #output folder for storage.

    # 150 x 150 mesh of a square domain
    domain_length = 40
    ncell = 600
    mesh = dl.RectangleMesh(dl.Point(0, 0), dl.Point(domain_length, domain_length), ncell, ncell)

    # Create linear Lagrange finite element
    P1 = dl.FiniteElement("Lagrange", mesh.ufl_cell(), 1)

    # Create the mixed space for the state space
    Vh_STATE = dl.FunctionSpace(mesh, P1*P1)

    # Create "the real number space" for the model parameters
    param_dim = 3
    Vh_PARAMETER = dl.VectorFunctionSpace(mesh, "R", degree=0, dim=param_dim)

    # Create the order parameter space for order parameters
    Vu_sub = Vh_STATE.extract_sub_space([0]).collapse()

    Vh = [Vh_STATE, Vh_PARAMETER]

    # Create the initial random state object
    scale = 1.0
    correlation_length = 0.001
    state_init = GRF_initial_guess(Vh[STATE], correlation_length, scale)

    # Create the forward problem object
    save = False #save the order parameter at each iteration. CAUTION! CAN TAKE UP A LOT OF SPACE!
    iter_solve = False #Enable iterative solver when the preconditioner is applicable. CAUTION! CAN LEAD TO DIVERGENCE IF ENABLED!
    pde = ok_min_prob(Vh, state_init, substrate = None, \
                      save=save, out_folder=out_folder, iter_solve = iter_solve)

    # Control the printing of the iteration data:
    # 1: Print information on screen
    # 2: Save data and visualization
    # <1: No printing
    pde.ok_min_solver.parameters["print_level"] = 1

    # Setting the number of double well backtracking iteration:
    pde.ok_min_solver.parameters["rescale_iterations"] = 3 # gamma = [1, 0.5, 0]

    # Setting the absolution tolerance for the residual norm:
    pde.ok_min_solver.parameters["abs_tolerance"] = 1e-10

    # Setting number of maximum iteration before giving up
    pde.ok_min_solver.parameters["maximum_iterations"] = 1000

    # Solve a particular problem
    x = [pde.generate_state(), pde.generate_parameter()]
    epsilon = 0.06
    sigma = 16
    m = -0.00
    x[PARAMETER].set_local(np.array([epsilon, sigma, m]))

    print("Solve forward problem...")
    pde.solveFwd(x[STATE], x)
    print("Finished.")

    # Extract the order parameter
    op = dl.Function(Vu_sub)
    pde.extract_order_parameter(op.vector(), x[STATE])

    # Plot the order parameter
    plt.figure(1)
    c = dl.plot(op, vmax = 1, vmin = -1, cmap = "Greys")
    plt.axis("off")
    plt.colorbar(c)
    plt.savefig(out_folder + "order_parameter_ref.pdf", bbox_inches = "tight")

    np.save(out_folder + "order_parameter_ref.npy", op.vector().get_local())

    npixel = 400
    print("Image size: " + str(npixel) + "x" + str(npixel))
    misfit = aaps_likelihood(Vh_STATE, npixel)

    c_1, c_2, sigma_b, sigma_n = 0.38, 0.15, 0.15, 0.012
    img = misfit.s2o.apply_local_averaging(x[STATE])
    img = misfit.s2o.apply_scaling(img, c_1, c_2)
    img = misfit.s2o.apply_blur(img, sigma_b)
    img += np.random.normal(scale = sigma_n, size = img.shape)

    plt.figure(2)
    c = plt.imshow(np.flip(img, axis = 0), cmap = "Greys", vmax = 1, vmin = 0)
    plt.axis("off")
    plt.colorbar(c)
    plt.clim(0, 1)
    plt.savefig(out_folder + "image_data_ref.pdf", bbox_inches = "tight")

    np.save(out_folder + "img_data_ref.npy", img)
    np.save(out_folder + "true_param_ref.npy", np.array([epsilon,sigma, m, c_1, c_2, sigma_b, sigma_n]))
