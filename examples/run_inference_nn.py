#!/usr/bin/env python
# coding: utf-8
from __future__ import absolute_import, division, print_function

import dolfin as dl
import sys, os
import numpy as np
import matplotlib.pyplot as plt
import math
import torch
sys.path.append(os.environ.get('HIPPYLIB_PATH'))
import hippylib as hl
sys.path.insert(0, '../')
from aaps_inference import *
sys.path.insert(0, '../../')
from dram import *

STATE = 0
PARAMETER = 1

import os
import argparse

def prior_m(param, data):
    c_1, c_2, sigma_n = param[3], param[4], param[-1]
    return -(-np.mean(data) - 2 * abs(c_1) * c_2 + c_2 + 0.5) / c_1, sigma_n / (400 * c_1)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='AAPS Inference')
    parser.add_argument('--data',
                        default="./data/img_data.npy",
                        type=str,
                        help="The path to data file")

    parser.add_argument('--parameters',
                        default="./data/true_param.npy",
                        type=str,
                        help="The path to true param file")

    parser.add_argument('--samples',
                        default=5000,
                        type=int,
                        help="Number of samples for each processer")

    parser.add_argument('--proc_id',
                        default=0,
                        type=int,
                        help="The processor ID")

    parser.add_argument('--out_path',
                        default='./',
                        type=str,
                        help="Relative path to store output files")

    parser.add_argument('--out_freq',
                        default=5,
                        type=int,
                        help="The frequency of saving results to .npy files")

    args = parser.parse_args()

    data_file = args.data
    param_file = args.parameters
    image_data = np.load(data_file)
    true_param = np.load(param_file)

    m_mean, m_std = prior_m(true_param, image_data)

    nx = 5
    mesh = dl.UnitSquareMesh(nx, nx)

    # Create linear Lagrange finite element
    P1 = dl.FiniteElement("Lagrange", mesh.ufl_cell(), 1)

    # Create the mixed space for the state space
    Vh_STATE = dl.VectorFunctionSpace(mesh, "R", degree=0, dim=100)

    # Create "the real number space" for the model parameters
    param_dim = 3
    Vh_PARAMETER = dl.VectorFunctionSpace(mesh, "R", degree=0, dim=param_dim)

    Vh = [Vh_STATE, Vh_PARAMETER]

    # Create the forward problem object
    pde = nn_surrogate_model(Vh)

    x = [pde.generate_state(), pde.generate_parameter()]

    prior = MixedPrior(Vh[PARAMETER])
    index_uniform = [0, 1]
    upper_uniform = np.array([1.0, 1.0])
    lower_uniform = np.array([0.0, 0.0])
    index_gaussian = [2]
    std_gaussian = np.array([m_std])
    mean_gaussian = np.array([m_mean])
    upper_gaussian = np.array([0.0])
    lower_gaussian = np.array([-1./math.sqrt(3)])
    prior.set_uniform_prior(index_uniform, upper_uniform, lower_bound=lower_uniform)
    prior.set_gaussian_prior(index_gaussian, np.diag(std_gaussian ** 2), mean_gaussian, \
                             upper_bound=upper_gaussian, lower_bound=lower_gaussian)

    noise, m_true = dl.Vector(), dl.Vector()
    prior.init_vector(noise, "noise")
    prior.init_vector(m_true, 1)

    proc_id = args.proc_id
    n_samples = args.samples
    out_freq = args.out_freq
    out_path = args.out_path

    model_mean, model_std = load_nn()

    misfit = nn_surrogate_likelihood(Vh, model_mean, model_std)
    observation = [None]
    observation[0] = image_data[image_data.shape[0] // 2 - 50:image_data.shape[0] // 2 + 50, \
                     image_data.shape[1] // 2 - 50:image_data.shape[1] // 2 + 50]
    misfit.set_observation_data(observation)
    misfit.set_observation_parameters(true_param[3], true_param[5]*4, true_param[6])

    model = hl.Model(pde, prior, misfit)

    proposal_std = np.zeros(3)
    proposal_std[0:2] = [0.05, 0.08]
    proposal_std[2] = m_std
    print(proposal_std)
    kernel = DRAMKernel(model, np.diag(proposal_std ** 2))

    kernel = DRAMKernel(model, np.diag(proposal_std ** 2))
    kernel.parameters["non-adaptive_samples"] = 20
    kernel.parameters["maximum_proposals"] = 3
    kernel.parameters["DR_proposal_scale"] = 0.2
    kernel.parameters["next_stage_probability"] = 0.5

    chain = DRAM_MCMC(kernel)
    chain.parameters["number_of_samples"] = n_samples
    chain.parameters["print_progress"] = n_samples // out_freq
    chain.parameters["print_level"] = -1

    tracer = Tracer(Vh, chain.parameters["number_of_samples"], chain.parameters["print_progress"], out_path)

    noise, m0 = dl.Vector(), dl.Vector()
    prior.init_vector(noise, 1)
    prior.init_vector(m0, 1)
    # Burn initial guess sample
    for i in range(proc_id):
        hl.parRandom.normal(1, noise)
        chain.consume_random()
    hl.parRandom.normal(1, noise)
    prior.sample(noise, m0)
    #m0.set_local(np.array([true_param[0] * 4, true_param[1] / 16., true_param[2], true_param[3], true_param[5] * 4, true_param[6]]) + proposal_std * noise.get_local())
    print(m0.get_local())

    # Start simulating a MCMC chain!
    n_accept, total_samples, total_fwd_solves = chain.run(m0, tracer)
    tracer.save()
    np.save(out_path + "chain_data.npy", np.array([n_accept, total_samples, total_fwd_solves]))
