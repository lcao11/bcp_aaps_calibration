#Author: Lianghao Cao
#Date: 03/14/2022

from __future__ import absolute_import, division, print_function

import numpy as np
import dolfin as dl

STATE, PARAMETER = 0, 1

class mean_likelihood:
    """
    The class implement the evaluation of negative log-likelihood function based on the mean pixal values of a raw data image.
    """

    def __init__(self, Vh_PARAMETER):
        """
        Constructor
        Inputs:
        - :code:`Vh_PARAMETER`:           The real valued vector space for the four parameters for inference.
        """
        self._Vh_PARAMETER = Vh_PARAMETER
        self._param_dim = Vh_PARAMETER.dim()
        if not self._param_dim == 4:
            raise ValueError("The parameter space dimension should be 4.")

    def set_observation_data(self, obs):
        self._obs_mean = np.mean(obs)
        self._Nx, self._Ny = obs.shape
        self._scale = 0.5*self._Nx*self._Ny
        self._set_obs = True

    def cost(self, x):
        if not self._set_obs:
            raise ValueError("The observation is not set.")
        m, c1, c2, sigma_n = x[PARAMETER].get_local()
        return self._scale/sigma_n**2*(c1*m - 2*abs(c1)*c2+ c2 + 0.5 - self._obs_mean)**2
