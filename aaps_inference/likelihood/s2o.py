#Author: Lianghao Cao
#Date: 02/21/2022

import numpy as np
import dolfin as dl
import math
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter

dl.parameters['linear_algebra_backend'] = 'PETSc'
dl.parameters["form_compiler"]["optimize"]     = True
dl.parameters["form_compiler"]["cpp_optimize"] = True
dl.parameters["form_compiler"]["representation"] = "uflacs"
dl.parameters["form_compiler"]["cpp_optimize_flags"] = "-O3 -ffast-math -march=native"

STATE = 0
PARAMETER = 1

class state2obs:
    def __init__(self, Vh_STATE, Nx, Ny, ratio = 1.0):
        """
        Constructor
        Inputs:
            - :code:`Vh`:                   The list consists of the (mixed) state space and the parameter space.
            - :code:`Nx`:                   The number of pixels in x direction
            - :code:`Ny`:                   The number of pixels in y direction. If not given then square images are assumed.
        """
        self._Vu = Vh_STATE.extract_sub_space([0]).collapse()
        l = np.max(self._Vu.mesh().coordinates())
        self._ratio = ratio
        if not self._ratio == 1.0:
            self._Nx_out = Nx
            self._Ny_out = Ny
            Nx = round(Nx*ratio)
            Ny = round(Ny*ratio)
        if self._Vu.dim() == (Nx+1)*(Ny+1):
            self._mismatch = False
        else:
            self._mismatch = True
            if Nx == Ny:
                mesh = dl.RectangleMesh(dl.Point(0.0, 0.0), dl.Point(l, l), Nx, Ny)
            else:
                mesh = dl.RectangleMesh(dl.Point(0.0, 0.0), dl.Point(l, Nx/np.float(Ny)*l), Nx, Ny)
            P1 = dl.FiniteElement("Lagrange", mesh.ufl_cell(), 1)
            self._Vu_img = dl.FunctionSpace(mesh, P1)
            self._func_img = dl.Function(self._Vu_img)
            self._help_img = dl.Function(self._Vu_img).vector()

        self._Nx = Nx
        self._Ny = Ny
        self._func = dl.Function(Vh_STATE)
        self._func_sub = dl.Function(self._Vu)

        self._help_sub = dl.Function(self._Vu).vector()

    def extract_order_parameter(self, op, state):
        """
        extract the order parameter from the state function in the mixed space.
        Inputs:
        - :code:`state`:                      The state (dolfin vector) in the (mixed) state space
        - :code:`order_parameter`:            The order parameter (dolfin vector) in the order parameter space.
        """
        self._func.vector().zero()
        self._func_sub.vector().zero()
        op.zero()
        self._func.vector().axpy(1., state)
        dl.assign(self._func_sub, self._func.sub(0))
        op.axpy(1., self._func_sub.vector())

    def project_state(self, output, input):

        self._func_sub.vector().zero()
        output.zero()
        self._func_sub.vector().axpy(1., input)
        func_img = dl.project(self._func_sub, V = self._Vu_img)
        output.axpy(1., func_img.vector())

    def state2img(self, state):
        if self._mismatch:
            self._func_img.vector().zero()
            self._func_img.vector().axpy(1., state)
            image = self._func_img.compute_vertex_values().reshape(self._Nx + 1, self._Ny + 1)
        else:
            self._func_sub.vector().zero()
            self._func_sub.vector().axpy(1., state)
            image = self._func_sub.compute_vertex_values().reshape(self._Nx + 1, self._Ny + 1)
        image_averaged = np.zeros((self._Nx, self._Ny))
        for i in range(self._Nx):
            for j in range(self._Ny):
                image_averaged[i, j] += image[i, j]
                image_averaged[i, j] += image[i, j + 1]
                image_averaged[i, j] += image[i + 1, j]
                image_averaged[i, j] += image[i + 1, j + 1]
        return image_averaged * 0.25

    def apply_local_averaging(self, u):
        self.extract_order_parameter(self._help_sub, u)
        if self._mismatch:
            self.project_state(self._help_img, self._help_sub)
            img = self.state2img(self._help_img)
        else:
            img = self.state2img(self._help_sub)
        return img

    def apply_scaling(self, img, c1, c2):

        return c1*img - 2*abs(c1)*c2 + c2 + 0.5

    def apply_blur(self, img, sigma_b):

        return gaussian_filter(img, 0.01*math.sqrt(img.shape[0]*img.shape[1])*sigma_b)

    def map(self, state, c1, c2, sigma_b):
        img = self.apply_local_averaging(state)
        img = self.apply_scaling(img, c1, c2)
        if not self._ratio == 1.0:
            img = self.apply_blur(img, sigma_b/self._ratio)
            return img[img.shape[0]//2 - self._Nx_out//2:img.shape[0]//2 + (self._Nx_out - self._Nx_out//2), \
                img.shape[1]//2 - self._Ny_out//2:img.shape[1]//2 + (self._Ny_out - self._Nx_out//2)]
        else:
            return self.apply_blur(img, sigma_b)


