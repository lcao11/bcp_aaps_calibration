# Author: Lianghao Cao
# Date: 02/21/2022

from __future__ import absolute_import, division, print_function

import numpy as np
import dolfin as dl
import math
from .s2o import state2obs
from .mean_nllh import mean_likelihood
from ..neural_network.nn_func import nn_prediction
from scipy.ndimage import gaussian_filter
from scipy.stats import ncx2
from scipy.stats import chi2
from scipy.stats import norm
from numpy.polynomial.hermite import hermgauss
from scipy.special import logsumexp
from scipy.special import erf
import torch

STATE, PARAMETER = 0, 1

np.seterr(divide='ignore')

def gaussian_weight(r, sigma_b):
    return -4 * math.pi ** 2 * r ** 2 * sigma_b ** 2 / 100 ** 2

def unique_radii(nx, ny):
    """
    Given a grid with
    Inputs:
    - :code:`nx`:               The number of gird points in x direction.
    - :code:`ny`:               The number of gird points in y direction.
    Output:
    (0). the numpy array of unique radii;
    (1). the numpy array of inversion indeces;
    (2). the numpy array of counts for each unique radii.
    """
    # Determine the radius of all entries in a full spectrum.
    y, x = np.indices((nx, ny))
    center_x = nx // 2
    center_y = ny // 2
    r = np.sqrt((x - center_x) ** 2 + (y - center_y) ** 2)

    return np.unique(r, return_inverse=True, return_counts=True)


def power_spectrum(img):
    """
    Inputs:
    - :code:`img`:                  A 2d numpy array.
    Output:
    - the power spectrum for :code:`img`.
    """
    return np.abs(np.fft.fftshift(np.fft.fft2(img))) ** 2


def extract_rps(ps, rfreq, idx, counts, rmax):
    """
    Inputs:
    - :code:`ps`:                A 2d numpy array corresponds to the full power spectrum
    - :code:`rfreq`:             A numpy array of the radial frequencies.
    - :code:`idx`:               A numpy array of the reversion index.
    - :code:`counts`:            A numpy array of the number of spectrum points at each radial frequency.
    Output:
    The radial power spectrum corresponds to :code:`img`:
    (1). A numpy array of all radial frequencies smaller than rmax
    (2). A numpy array of azimuthally-averaged power spectrum values at each radial frequency.
    (3). A numpy array of the number of spectrum points at each radial frequency.
    """
    out_values = np.zeros(rfreq.size)
    for i in range(idx.size):
        if rfreq[idx[i]] <= rmax:
            out_values[idx[i]] += ps.ravel()[i]
    out_rfreq = rfreq[1:]
    out_values = out_values[1:]
    out_c = counts[1:]
    out_values = np.divide(out_values, out_c.astype(float))
    return out_rfreq[out_rfreq <= rmax], out_values[out_rfreq <= rmax], out_c[out_rfreq <= rmax]


def extract_averaged_rps(rfreq, rps, counts, n_h, h_r, index_set=None):
    """
    Inputs:
    - :code:`rfreq`:             A numpy array of the radial frequencies.
    - :code:`rps`:               A numpy array of the radial power spectrum
    - :code:`counts`:            A numpy array of the average points for each radial frequencies
    - :code:`n_h`:               A integer of number of annulus for the averaging
    - :code:`h_r`:               A floating number of width of all annulus
    Output:
    The annulus-averaged power spectrum based on the full azimuthally-averaged power spectrum
    (1). A numpy array of radial frequencies at the averged center of each annulus.
    (2). A numpy array of annulus-averaged power spectrum values.
    (3). A numpy array of the number of spectrum points used to computed each averaged spectrum.
    """
    out_freqs = np.zeros(n_h)
    out_values = np.zeros(n_h)
    out_counts = np.ones(n_h).astype(int)
    if index_set is None:
        index_set = np.arange(n_h)
    for j in index_set:
        r = j * h_r
        for i in range(rfreq.size):
            if r < rfreq[i] <= r + h_r:
                out_freqs[j] += rfreq[i] * counts[i]
                out_counts[j] += counts[i]
                out_values[j] += rps[i] * counts[i]
    out_freqs = np.divide(out_freqs, out_counts.astype(float))
    out_values = np.divide(out_values, out_counts.astype(float))
    return out_freqs, out_values, out_counts


class aaps_likelihood:
    """
    The class implement the evaluation of negative log-likelihood function based on the azimuthally-averaged power spectrum given a raw data image.
    """

    def __init__(self, Vu, Nx, ratio = 1.0, Ny=None, r_max=None, n_h=None, normal=False):
        """
        Constructor
        Inputs:
        - :code:`Vu`:                     The mixed Finite element space on which the solution vector is defined in the first component.
        - :code:`nx`:                     The number of cells in x direction for the mesh for :code:`Vh`:.
        - :code:`ny`:                     The number of cells in y direction for the mesh for :code:`Vh`:.\
                                          If not provided, a square domain is assumed.
        - :code:`r_max`:                  The maximum frequency to use for the likelihood estimation.\
                                          If not provided, the full azimuthally-averaged spectrum is used to compute the NLLH.
        - :code:`n_h`:                    The number of intervals for averaging the azimuthally-averaged spectrum data.\
                                          If not provided, the full azimuthally-averaged spectrum is used to compute the NLLH.
        """

        Vu_sub = Vu.extract_sub_space([0]).collapse()
        self._Nx = Nx
        if Ny is None:
            self._Ny = Nx
        else:
            self._Ny = Ny

        self._rfreq, self._idx, self._c = unique_radii(self._Nx, self._Ny)

        if r_max is None:
            self._r_max = self._rfreq[-1]
        else:
            self._r_max = r_max

        self._n_h = n_h
        if not (self._n_h is None):
            self._h_r = self._r_max / np.float(self._n_h)

        self._data_index = None
        self._set_parameters = False
        self._normal = normal

        self.s2o = state2obs(Vu, self._Nx, self._Ny, ratio=ratio)

    def radial_power_spectrum(self, img):
        """
        Compute the radial power spectrum of a given image
        Inputs:
        - :code:`img`:                A 2d numpy array. Must have one of the axis longer than 2*r_max
        Output:
        The radial power spectrum corresponds to :code:`img`:
        (1). A numpy array of the radial frequencies, $\mathbf{r}$.
        (2). A numpy array of the reversion index.
        (3). A numpy array of the average points for each radial frequencies, $N_{\theta}(\mathbf{r}_k)$.
        """
        ps = power_spectrum(img)
        return extract_rps(ps, self._rfreq, self._idx, self._c, self._r_max)

    def set_observation_data(self, obs):
        """
        Setting the observation data and the white noise standard deviation.
        Inputs:
        - :code:`image_data`:                  A 2d numpy array. Must have one of the axis longer than 2*r_max.
        """

        if not (obs.shape[0] == self._Nx and obs.shape[1] == self._Ny):
            raise ValueError("The observation does not match the pre-assigned dimension")

        rfreq, rps, counts = self.radial_power_spectrum(obs)
        if self._n_h is None:
            self.rfreq = rfreq
            self.data_rps = rps
            self.counts = counts
        else:
            self.rfreq, self.data_rps, self.counts = extract_averaged_rps(rfreq, rps, counts, self._n_h, self._h_r)

        self._noise_scale = 2 * self.counts / float(self._Nx * self._Ny)
        self._llh_x = self.data_rps * self._noise_scale

        self._set_obs = True

    def set_observation_index(self, index_array):
        self._data_index = index_array

    def set_observation_parameters(self, c1, sigma_b, sigma_n):
        self._c1 = c1
        self._sigma_b = sigma_b
        self._sigma_n = sigma_n

        self._set_parameters = True

    def aaps_nllh(self, rps, sigma_n):
        """
        Evaluate the log-likelihood based on the non-central chi-square distribution.
        Inputs:
        - :code:`rps`:                  A numpy array corresponds to the radial power spectrum.
        Output:
        The value of the log-likelihood at :code:`rps`.
        """
        if not self._set_obs:
            raise ValueError("The observation is not set.")
        sum = 0.0
        for i in self._data_index:
            x = self._llh_x[i] / sigma_n ** 2
            df = 2 * self.counts[i]
            nc = rps[i] * self._noise_scale[i] / sigma_n ** 2
            log_pdf = 0
            if not self._normal:
                log_pdf = ncx2.logpdf(x, df, nc)
            if self._normal or math.isinf(log_pdf):
                sum -= norm.logpdf(x, loc=nc + df, scale=math.sqrt(2 * df + 4 * nc))
            else:
                sum -= log_pdf
        return sum

    def cost(self, x):
        """
        Evaluate the log-likelihood based on the non-central chi-square distribution at the given parameter.
        Inputs:
        - :code:`x`:                  A 3 tuple with the first element being a dolfin vector from :code:`Vh`.
        Output:
        The value of the log-likelihood at :code:`x`.
        """
        if not self._set_parameters:
            raise ValueError("The characterizatiom model parameters are not set.")
        img = self.s2o.map(x[STATE], self._c1, 0.0, self._sigma_b)
        rfreq, rps, counts = self.radial_power_spectrum(img)

        if self._data_index is None:
            if self._n_h is None:
                self._data_index = np.arange(rps.size)
            else:
                self._data_index = np.arange(self._n_h)

        if not self._n_h is None:
            _, averaged_rps, _ = extract_averaged_rps(rfreq, rps, counts, \
                                                      self._n_h, self._h_r, index_set=self._data_index)
            return self.aaps_nllh(averaged_rps, self._sigma_n)
        else:
            return self.aaps_nllh(rps, self._sigma_n)


class nn_surrogate_likelihood:
    """
    The class implement the evaluation of negative log-likelihood function based on the azimuthally-averaged power spectrum given a raw data image.
    """

    def __init__(self, Vh, model_mean, model_std):
        """
        Constructor
        None.
        """
        self.Vh = Vh
        if not self.Vh[STATE].dim() == 100:
            raise ValueError("The state space dimension should be 100")
        self._Nx = 100
        self._Ny = 100

        self._rfreq, self._idx, self._c = unique_radii(self._Nx, self._Ny)

        self._r_max = 50
        self._n_h = 50
        self._h_r = 1.0
        self._data_index = None
        self._set_parameters = False
        self._x_hg, self._w_hg = hermgauss(50)

        self._model_mean = model_mean
        self._model_std = model_std

    def radial_power_spectrum(self, img):
        """
        Compute the radial power spectrum of a given image
        Inputs:
        - :code:`img`:                A 2d numpy array. Must have one of the axis longer than 2*r_max
        Output:
        The radial power spectrum corresponds to :code:`img`:
        (1). A numpy array of the radial frequencies.
        (2). A numpy array of the azimuthally-averaged power spectrum.
        (3). A numpy array of the average points at the radial frequencies.
        """
        ps = power_spectrum(img)
        return extract_rps(ps, self._rfreq, self._idx, self._c, self._r_max)

    def set_observation_data(self, obs):
        """
        Setting the observation data and the white noise standard deviation.
        Inputs:
        - :code:`obs`:                  A 2d numpy array. Must have size of 100x100
        - :code:`sigma_n`:              A positive floating point number representes wide noise std
        """
        self._n_obs = len(obs)
        self.data_rps = np.zeros((self._n_h, self._n_obs))
        self._llh_x = np.zeros((self._n_h, self._n_obs))
        for i in range(self._n_obs):
            rfreq, rps, counts = self.radial_power_spectrum(obs[i])
            if i == 0:
                self.rfreq, self.data_rps[:, i], self.counts = extract_averaged_rps(rfreq, rps, counts, self._n_h,
                                                                                    self._h_r)
            else:
                _, self.data_rps[:, i], _ = extract_averaged_rps(rfreq, rps, counts, self._n_h, self._h_r)
        self._noise_scale = 2 * self.counts / (self._Nx * self._Ny)
        for i in range(self._n_obs):
            self._llh_x[:, i] = self.data_rps[:, i] * self._noise_scale

        self._set_obs = True

    def set_observation_index(self, index_array):
        """
        Evaluate the log-likelihood based on the non-central chi-square distribution.
        Inputs:
        - :code:`rps`:                  A numpy array corresponds to the radial power spectrum.
        Output:
        The value of the log-likelihood at :code:`rps`.
        """
        self._data_index = index_array

    def set_observation_parameters(self, c1, sigma_b, sigma_n):
        self._c1 = c1
        self._sigma_b = sigma_b
        self._sigma_n = sigma_n

        self._set_parameters = True

    def aaps_nllh(self, mean, std):
        """
        Evaluate the log-likelihood based on the non-central chi-square distribution.
        Inputs:
        - :code:`rps`:                  A numpy array corresponds to the radial power spectrum.
        Output:
        The value of the log-likelihood at :code:`rps`.
        """
        if (not self._set_obs) and (not self._set_parameters):
            raise ValueError("The observations or parameters are not set.")
        sum = 0.0
        scale = 2 * np.log(2 * self._c1) + gaussian_weight(6, self._sigma_b)
        #scale = 0.0
        weight = 1.0 - 0.5 * (1 + erf(-mean / (std * math.sqrt(2))))
        #weight = 1.0*np.ones(std.size)
        for k in range(self._n_obs):
            for i in self._data_index:
                position = np.sqrt(2) * self._x_hg * std[i] + mean[i]
                x = self._llh_x[i, k] / self._sigma_n ** 2
                df = 2 * self.counts[i]
                #nc = np.exp(position + np.log(self._noise_scale[i] / self._sigma_n ** 2) + scale)
                nc = position*self._noise_scale[i] / self._sigma_n ** 2*np.exp(scale)
                value = np.zeros(position.size)
                for j in range(position.size):
                    if nc[j] <= 0:
                        value[j] = -math.inf
                    else:
                        value[j] = ncx2.logpdf(x, df, nc[j])
                        if math.isinf(value[j]):
                            value[j] = norm.logpdf(x, loc=nc[j] + df, scale=math.sqrt(2 * df + 4 * nc[j]))
                sum -= logsumexp(value, b=self._w_hg/(weight[i]*math.sqrt(math.pi)))
        return sum

    def cost(self, x):
        """
        Evaluate the log-likelihood based on the non-central chi-square distribution at the given parameter.
        Inputs:
        - :code:`x`:                  A 2 tuple with the first element being the real value vector state space.
        Output:
        The value of the log-likelihood at :code:`x`.
        """

        if self._data_index is None:
            self._data_index = np.arange(self._n_h)
        if (not self._set_obs) and (not self._set_parameters):
            raise ValueError("The observations or parameters are not set.")

        sample = np.zeros((1, 5))
        sample[0, :3] = x[PARAMETER].get_local()
        sample[0, 3] = self._c1
        sample[0, 4] = self._sigma_b
        pred_mean, pred_std = nn_prediction(self._model_mean, self._model_std, sample)

        return self.aaps_nllh(pred_mean.flatten(), pred_std.flatten())
