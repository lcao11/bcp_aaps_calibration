from __future__ import absolute_import, division, print_function

import dolfin as dl
import numpy as np
import math
from scipy.special import logsumexp
import sys, os
sys.path.append(os.environ.get('DRAM_PATH'))
from dram import *

class PMMHKernel(DRAMKernel):
    def __init__(self, model, problem_parallel, proposal_covariance):
        super(PMMHKernel, self).__init__(model, proposal_covariance)
        self.parameters["number_of_particles"] = 1
        self.problem_parallel = problem_parallel

    def name(self):
        return "Pseudo-Marginal Adpative Metropolis-Hasting with Delayed Rejection"

    def init_sample(self, current):
        cost = np.zeros(self.parameters["number_of_particles"])
        u0_list = [self.model.problem.generate_state() for i in range (self.parameters["number_of_particles"])]
        if not math.isinf(self.model.prior.cost(current.m)):
            self.problem_parallel.solveMP(u0_list, current.m)
            for i in range(self.parameters["number_of_particles"]):
                current.u.zero()
                current.u.axpy(1., u0_list[i])
                cost[i] = self.model.cost([current.u,current.m,None])[0]
            current.cost -= logsumexp(-cost, b = 1./self.parameters["number_of_particles"])
        else:
            current.cost = math.inf

    def consume_random(self):
        for i in range(self.parameters["maximum_proposals"]*2):
            np.random.rand()
        np.random.multivariate_normal(np.zeros(self._dim), np.diag(np.ones(self._dim)))
        for i in range(self.parameters["number_of_particles"]):
            self.model.problem.consume_random()
