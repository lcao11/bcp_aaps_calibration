import matplotlib.pyplot as plt
import numpy as np
import dolfin as dl
import sys, os
sys.path.append(os.environ.get('HIPPYLIB_PATH'))
import hippylib as hl
from aaps_inference import *
sys.path.insert(0, '../../ok_newton')
from OKNewton import *
sys.path.insert(0, '../../')
from dram import *
import multiprocessing as mp
import itertools

STATE, PARAMETER = 0, 1

class ok_forward_parallel:
        def __init__(self, forward_settings):
                self.domain_length = forward_settings["domain_length"]
                self.n_cells = forward_settings["n_cells"]
                self.param_dim = forward_settings["param_dim"]
                self.scale = forward_settings["scale"]
                self.correlation_length = forward_settings["correlation_length"]
                self.n_proc = forward_settings["n_proc"]

        def make_spaces(self):
                mesh = dl.RectangleMesh(dl.Point(0, 0), dl.Point(self.domain_length, self.domain_length),\
                                        self.n_cells, self.n_cells)
                # Create linear Lagrange finite element
                P1 = dl.FiniteElement("Lagrange", mesh.ufl_cell(), 1)
                # Create the mixed space for the state space
                Vh_STATE = dl.FunctionSpace(mesh, P1 * P1)
                # Create "the real number space" for the model parameters
                Vh_PARAMETER = dl.VectorFunctionSpace(mesh, "R", degree=0, dim=self.param_dim)
                Vh = [Vh_STATE, Vh_PARAMETER]
                return Vh

        def solve(self, m_array, proc_id):
                """ Solve the state problem in terms of arrays """
                Vh = self.make_spaces()
                state_init = GRF_initial_guess(Vh[STATE], self.correlation_length, self.scale)
                u0 = dl.Function(Vh[STATE]).vector()
                m = dl.Function(Vh[PARAMETER]).vector()
                x = [u0, m]
                m.set_local(m_array)
                pde = ok_min_prob(Vh, state_init)
                pde.ok_min_solver.parameters["print_level"] = -1
                # Setting the number of double well backtracking iteration:
                pde.ok_min_solver.parameters["rescale_iterations"] = 3  # gamma = [1, 0.5, 0]
                # Setting the absolution tolerance for the residual norm:
                pde.ok_min_solver.parameters["abs_tolerance"] = 1e-10
                pde.ok_min_solver.parameters["rel_tolerance"] = 1e-10
                # Setting number of maximum iteration before giving up
                pde.ok_min_solver.parameters["maximum_iterations"] = 1000
                u = pde.generate_state()
                print("start solve")
                for i in range(proc_id):
                        pde.consume_random()
                pde.solveFwd(u, x)
                for i in range(self.n_proc - proc_id):
                        pde.consume_random()

                u_func = dl.Function(Vh[STATE])
                u_func.vector().zero()
                u_func.vector().axpy(1., u)
                plt.figure(proc_id)
                dl.plot(u_func.split(deepcopy=True)[0], vmax = 1, vmin = -1, cmap = "Greys")
                plt.savefig("./run_m0/solution_" + str(proc_id) + ".pdf", bbox_inches = "tight")
                plt.close()
                return u.get_local()


        def solveMP(self, u_out_list, m):
                m_array = m.get_local()
                with mp.Pool(self.n_proc) as p:
                        results = p.starmap(self.solve, zip(itertools.repeat(m_array), np.arange(self.n_proc)))
                for i in range(len(results)):
                        u_out_list[i].set_local(results[i])

                exit()

