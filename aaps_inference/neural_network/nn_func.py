import torch
from .nn_model import Net
import numpy as np
import math
import sys, os


def load_nn():

    mean_filename = os.path.dirname(os.path.realpath(__file__)) + "/mean.pt"
    std_filename = os.path.dirname(os.path.realpath(__file__)) + "/std.pt"

    mean_checkpoint = torch.load(mean_filename)

    mean_models = []
    output_dim = 50
    train_dim = 50

    model_mean = Net()
    model_mean.load_state_dict(torch.load(mean_filename))
    model_mean.eval()
    model_std = Net()
    model_std.load_state_dict(torch.load(std_filename))
    model_std.eval()

    return model_mean, model_std


def rescale(mean, std):
    maxmin = np.load(os.path.dirname(os.path.realpath(__file__)) + "/data/maxmin.npy")
    return mean * (maxmin[0, 0] - maxmin[0, 1]) + maxmin[0, 1], std * (maxmin[1, 0] - maxmin[1, 1]) + maxmin[1, 1]


def nn_prediction(mean_model, std_model, sample):
    output_dim = 50
    train_dim = 50
    sample[:, 2] = np.abs(sample[:, 2])

    mean_pred = np.zeros((sample.shape[0], output_dim))
    std_pred = np.zeros((sample.shape[0], output_dim))

    x = torch.tensor(sample)
    with torch.no_grad():
        mean_pred = mean_model(x.float()).numpy()
        std_pred = std_model(x.float()).numpy()

    # return mean_pred, std_pred
    # return rescale(mean_pred, std_pred)
    rescaled_mean, rescaled_std = rescale(mean_pred, std_pred)
    return np.exp(rescaled_mean), np.exp(rescaled_std)
