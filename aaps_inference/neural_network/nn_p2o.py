#Author: Lianghao Cao and Keyi Wu
#Date of creation: 03/20/2022
#Purpose: The neural network surrogate forward operator for the calibration of Ohta--Kawasaki model with\
#   azimuthally-averaged power spectrum.

import numpy as np
import dolfin as dl

STATE, PARAMETER = 0, 1

class nn_surrogate_model:
    """
    The class implements the neural network surrogate of the parameter-to-observable map based on\
     the azimuthally-averaged power spectrum of image data and the Ohta--Kawasaki model
    """

    def __init__(self, Vh):
        """
        Constructor
        Inputs:
        - :code:`Vh`:       The list consists of the real vector state space (dim 71) and the parameter space.
        """
        self.Vh = Vh

        if not self.Vh[STATE].dim() == 100:
            raise ValueError("The dimension of the real value vector dolfin space should be " + str(100) + ".")

    def generate_state(self):
        """ Return a vector in the shape of the state. """
        return dl.Function(self.Vh[STATE]).vector()

    def generate_parameter(self):
        """ Return a vector in the shape of the parameter. """
        return dl.Function(self.Vh[PARAMETER]).vector()

    def solveFwd(self, state, x):

        #output_array_mean = self._model_mean(x[PARAMETER].get_local())
        # output_array_std = Neural_Network_Std_Evaluation(x[PARAMETER].get_local())
        # state.set_local(output_array_mean + np.random.normal(scale = out_put_array_std))
        pass



