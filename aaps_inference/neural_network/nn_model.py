import torch

class Net(torch.nn.Module):

    def __init__(self):
        super(Net, self).__init__()
        self.dim_input = 5
        self.dim_output = 50  # output_train.shape[0]  # dimension of output layer
        self.dim_inner1 = 20  # dimension of inner layers
        self.dim_inner2 = 35  # dimension of inner layers

        self.layers = torch.nn.Sequential(
                torch.nn.Linear(self.dim_input, self.dim_inner1),
                torch.nn.ELU(),  # nn.Softplus(),#nn.ReLU(),
                torch.nn.Linear(self.dim_inner1, self.dim_inner2),
                torch.nn.ELU(),  # nn.Softplus(),#nn.ReLU(),
                torch.nn.Linear(self.dim_inner2, self.dim_output)
            )

    def forward(self, x):
        return self.layers(x)
