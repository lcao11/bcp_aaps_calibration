import numpy as np
import sys, os
import argparse
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import pickle
import random
import torch
from torch.utils.data import TensorDataset
from torch.utils.data import DataLoader
# Set fixed random number seed
torch.manual_seed(0)
random.seed(0)
from nn_model import *

plt.rc('text', usetex=True)
plt.rc('font', family='serif', size = 15)
matplotlib.rcParams['text.latex.preamble']= r"\usepackage{amsmath}"


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Neural Network Training')
    parser.add_argument('--learning_rate',
                        default=1.e-3,
                        type=float,
                        help="The learning rate of the neural nets")
    parser.add_argument('--component',
                        default="mean",
                        type=str,
                        help="The component for training")
    parser.add_argument('--epochs',
                        default=5001,
                        type=int,
                        help="The number of epoch")
    parser.add_argument('--n_layers',
                        default= 6,
                        type=int,
                        help="The number of layers")
    parser.add_argument('--activation',
                        default= 0,
                        type=int,
                        help="The activation function type")
    parser.add_argument('--out_folder',
                        default="./",
                        type=str,
                        help="The output file folder")
    parser.add_argument('--print_level',
                        default=-1,
                        type=int,
                        help="The amount of information print to screen")


    args = parser.parse_args()
    out_folder = args.out_folder
    if not os.path.isdir(out_folder):
        os.mkdir(out_folder)
    learning_rate = args.learning_rate
    component = args.component
    epochs = args.epochs
    print_level = args.print_level

    input_train = np.load("./data/train_param.npy")
    input_valid = np.load("./data/valid_param.npy")
    output_train = np.load("./data/train_" + component + ".npy")
    output_valid = np.load("./data/valid_" + component + ".npy")

    model = Net()
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

    train_loss = []
    valid_loss = []

    # batch size
    bs = 64
    x_train, y_train, x_valid, y_valid = map(torch.tensor, (input_train, output_train, input_valid, output_valid))
    x_train, y_train, x_valid, y_valid = x_train.float(), y_train.float(), x_valid.float(), y_valid.float()
    train_ds = TensorDataset(x_train, y_train)
    train_dl = DataLoader(train_ds, batch_size=bs, shuffle = True)

    valid_ds = TensorDataset(x_valid, y_valid)
    valid_dl = DataLoader(valid_ds, batch_size=2*bs, shuffle = True)

    params = list(model.parameters())
    loss_func = torch.nn.MSELoss(reduction='mean')#s(reduction='sum')
    # def loss_func(output, target):
    # 	#loss = torch.mean((output - target) ** 2*torch.tensor(np.log(pixel)))
    # 	loss = torch.mean(((torch.exp(output) - torch.exp(target))/torch.exp(target)) ** 2 / torch.tensor(pixel))
    # 	return loss

    # Run the training loop
    for epoch in range(epochs):

        # Set current loss value
        current_loss = 0.0

        # Iterate over the DataLoader for training data
        for i, data in enumerate(train_dl, 0):

            # Get and prepare inputs
            inputs, targets = data
            inputs, targets = inputs.float(), targets.float()

            # Zero the gradients
            optimizer.zero_grad()

            # Perform forward pass
            outputs = model(inputs)

            # Compute loss
            loss = loss_func(outputs, targets)

            # Perform backward pass
            loss.backward()

            # Perform optimization
            optimizer.step()

            # Print statistics
            current_loss += loss.item()
        if epoch % 100 == 0:
            if print_level > 0:
                print('Train Loss after epoch', epoch + 1, current_loss)
            train_loss.append(current_loss)
            with torch.no_grad():
                current_loss = 0.0
                for i, data in enumerate(valid_dl, 0):
                    inputs, targets = data
                    inputs, targets = inputs.float(), targets.float()
                    # Perform forward pass
                    outputs = model(inputs)

                    # Compute loss
                    loss = loss_func(outputs, targets)
                    current_loss += loss.item()
                if print_level > 0:
                    print('Valid Loss after epoch', epoch + 1, current_loss)
                valid_loss.append(current_loss)

    # Process is complete.
    if print_level > 0:
        print('Training process has finished.')

    plt.figure(1)
    fig, ax = plt.subplots(figsize=(7, 5))
    ax.plot(np.arange(0, epochs, 100), train_loss, color = 'C0', label=r'Training')
    ax.plot(np.arange(0, epochs, 100), valid_loss, color = 'C1', label=r'Validation')
    plt.xlabel(r'Epoch')
    plt.ylabel(r'Loss')
    ax.set_yscale('log')
    plt.legend()
    plt.savefig(out_folder + "evolution_training.pdf", bbox_inches='tight')
    plt.close()

    np.save(out_folder + "training_loss.npy", np.array(train_loss))
    np.save(out_folder + "validation_loss.npy", np.array(valid_loss))

    filename = out_folder + component + ".pt"
    torch.save(model.state_dict(), filename)

    y0_pred= np.zeros((input_train.shape[0],50))
    y_pred = np.zeros((input_valid.shape[0],50))

    with torch.no_grad():
        y0_pred[:, :] = model(torch.tensor(input_train).float())
        y_pred[:, :] = model(torch.tensor(input_valid).float())


    error_mean = []
    for i in range(len(output_train)):
        error_mean.append(np.linalg.norm(output_train[i] - y0_pred[i]) / np.linalg.norm(output_train[i]))
    if print_level > 0:
        print("train err:")
        print("rel error", np.mean(error_mean), np.max(error_mean))


    error_mean = []
    for i in range(len(output_valid) - input_valid.shape[0], len(output_valid)):
        error_mean.append(np.linalg.norm(output_valid[i] - y_pred[i]) / np.linalg.norm(output_valid[i]))
    if print_level > 0:
        print("valid err:")
        print("rel error", np.mean(error_mean), np.max(error_mean))

    plt.subplot(211)
    hist, bins, _ = plt.hist(error_mean, bins=30)
    logbins = np.logspace(np.log10(bins[0]), np.log10(bins[-1]), len(bins))
    plt.subplot(212)
    plt.hist(error_mean, bins=logbins)
    plt.xlabel(r"Relative error")
    plt.ylabel(r"Count")
    plt.xscale('log')

    plt.savefig(out_folder + "error_distribution.pdf", bbox_inches="tight")
    plt.close()

